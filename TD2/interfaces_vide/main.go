package main

import "fmt"

func main() {

PrintIt(int(1))
}

func PrintIt(i interface{}) {
	switch v := i.(type) {
	case int:
		fmt.Println("The type is int !")
	case string:
		fmt.Println("The type is string !")
	default:
		fmt.Printf("I don't know about type %T!\n", v)
	}

}
