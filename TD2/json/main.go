package main

import (
	"log"
	"encoding/json"
	"net/http"
	"io/ioutil"
)

//déclaration de la map global
var usersMap = make(map[string]User)

// Structure
type User struct {
	Login    string `json:"userName"`
	Password string
	ID	 string `json:"userID"`
}

//fonction main
func main() {

	//lire le json
	data, err := ioutil.ReadFile("./users.json")
	if err != nil {
		log.Fatal(err)
	}
	
	var fileUsers[]User
	err = json.Unmarshal(data, &fileUsers)
	if err != nil {
		log.Fatal(err)
	}
	
	for _, u := range fileUsers {
		usersMap[u.ID] = u
	}
	
	//lancer un srv web
	http.HandleFunc("/", handler)
	http.ListenAndServe("8080", nil)
}

// fonction handler
func handler(w http.ResponseWriter, r *http.Request) {
	valeurID := r.FormValue("id")
	
	if valeurID == "" {
		return
	}

	w.Header().Set(
		"Content-Type",
		"applicaton/json; charset=utf-8",
	)

	user, found := usersMap[valeurID]
	if !found {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	data, err := json.Marshal(user)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	
	_, err = w.Write(data)
	if err != nil {
		log.Println("je n'arrive pas envoyer la réponse")
		return
	}	
	

}
