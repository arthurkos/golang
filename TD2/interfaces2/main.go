package main

import "fmt"
import "strconv"
import "strings"

type IPAddr [4]byte

func main() {
	hosts := map[string]IPAddr{
		"loopback":  {127, 0, 0, 1},
		"googleDNS": {8, 8, 8, 8},
	}
	for name, ip := range hosts {
		fmt.Printf("%v: %v\n", name, ip)
	}
}

func (ip IPAddr) String() string {
	var result []string

	for _, elt := range ip {
		result = append(result, strconv.Itoa(int(elt)))
	}
	return strings.Join(result, ".")

}
