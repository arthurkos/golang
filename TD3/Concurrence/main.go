package main

import (
	"fmt"
	"sync"
)

var wg sync.WaitGroup

func main() {
	wg.Add(1)
	go maFonction()
	wg.Wait()
	fmt.Println("Fin du programme")
}

func maFonction(){
	defer wg.Done()
	fmt.Println("j'ai fini !")
}
