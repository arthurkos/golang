# golang

On va réaliser un petit serveur HTTP de liste de tâches.  Quand le pro-gramme sera terminé, vous pourrez ajouter des tâches, en faire la liste, et mar-quer des tâches comme terminées, en utilisant des requêtes HTTP.

## 1  La base du serveur
1. Créez un nouveau projet go.
2. Définissez une fonction main, qui sera la base de l’exécution de votreprogramme.
3. Définissez un type Task, qui est une struct avec deux champs:
- ”Description”, de type string
- ”Done”, de type bool
4. Définissez une variable globale ”tasks” qui est une slice de Task.5.Définissez trois HandleFunc (https://golang.org/pkg/net/http/#HandleFunc)et donnez-leur respectivement:
- Un path ”/”, et une fonction ”list”
- Un path ”/done”, et une fonction ”done”
- Un path ”/add”, et une fonction ”add”
6. Appelez, dans le main, la fonction ListenAndServe(https://golang.org/pkg/net/http/#ListenAndServe) qui vous permettrade lancer votre serveur HTTP avec les handlers que vous venez de définir

## 2  La fonction ”list
1. Définissez une fonction ”list”, c’est celle que vous avez passé au handleravec le path ”/”, avec la signature suivante:func list(rw http.ResponseWriter, _ *http.Request)(notez qu’on ne se servira pas de la Request)
2. Générez une chaîne de caractère à partir de la liste de vos tâches, qui setrouve dans la variable globale ”tasks” que vous avez définie plus tôt.C’est à dire que vous devez fabriquer une variable de type string, quireprésente votre liste de tâches. Pensez à inclure un ID pour votre tâche.Cet ID est l’indice de la tâche dans la slice tasks.Exemple de string à générer.
> ID: 0, task:"Fairelescourses"
> ID: 1, task:"Payerlesfactures"

si la slice est:
> _[{"Fairelescourses"false},
> {"Payerlesfactures"false}]_


3. Ajoutez une condition au code précédent pour n’inclure à l’aﬀichage queles tâches qui ne sont pas terminées (celles pour lesquelles ”Done” ==false).
4. Écrivez le header de la réponse à envoyer au client

> :rw.WriteHeader(http.StatusOK)
5. Puis écrivez votre réponse dans rw, avec:
Write([]byte) (int, error)
Notez que vous avez actuellement une chaîne de caractères et que Writeprend une slice de byte. Vous pouvez convertir la chaîne en slice de byte avec:
> _chaine:="machaine"
> enBytes:= []byte(chaine)_


## 3  La fonction add
La fonction add ne va accepter que des requêtes HTTP ”POST”. On n’attendradans ces requêtes (dans le body) qu’un petit descriptif de la tâche qu’on veutajouter à la liste, sous forme de chaîne de caractères.
1. Commencez par vérifier que la méthode de r (la variable *http.Request dela fonction pour la route ”/add”), est bien http.MethodPost.
La http.MethodPost est une constante:
https://golang.org/pkg/net/http/#pkg-constants
Vous pouvez récupérer la méthode qui est dans le champ Methodde la variable r
Documentation dutypede r:https://golang.org/pkg/net/http/#pkg-constants
2. Si la méthode n’est pas POST, écrivez le status ”bad request” dans votreréponse, et terminez la fonction:
rw.WriteHeader(http.StatusBadRequest)
3. À la suite, lisez le corps de la requête avec:
> body, err := ioutil.ReadAll(r.Body)
>   if err != nil {fmt.Printf("Errorreadingbody:%v", err)
>   http.Error(
>       rw,
>       "can'treadbody", http.StatusBadRequest,
>    )return
>   } 

Le body ainsi récupéré est une slice de byte.  Castez-le en string, puisplacez la chaîne de caractères ainsi obtenue dans un nouvel objet Task,dans le champ ”Description”.
4. Ajoutez la tâche que vous venez de fabriquer avec ”append”:
> https://tour.golang.org/moretypes/15
5. Écrivez le status ”OK” dans votre réponse HTTP, si tout s’est bien passé
6. Pour tester un add, vous pouvez utiliser la commande curl suivante (sivotre serveur écoute sur le port 8080):
> curl --request POST --data'Description'http://localhost:8080/add

## 4 La fonction done
On va ajouter ici un moyen de marquer une tâche comme terminée, ainsi que den’aﬀicher que les tâches terminées. Il s’agit donc à présent de mettre du codedans la fonction ”done”, qui est utilisée pour la route ”/done”.
1. Faites un switch sur la valeur de r.Method
2. Pour le cas où r.Method est égal à http.MethodGet, écrivez du code simi-laire à l’aﬀichage des tâches dans list, mais filtrez les tâches pour n’aﬀicherque celles qui sont à Done==true.
3. Pour le cas où r.Method est égal à http.MethodPost, lisez le body de rcomme précédemment, et convertissez-le en entier.
4. Utilisez l’entier pour récupérer la tâche dans la variable globale tasks (on rappelle que cet entier est la position de la tâche dans la slice), et changezDone à ”true”.(attention à bien vérifier que l’indice n’est pas plus grand que le tableausinon le programme panique ! Vous pouvez utiliser len(tasks) pour avoir le nombre de vos tasks)
5. Pour le cas ”default” de votre switch, renvoyez un status ”BadRequest”comme précédemment.
6. Vous pouvez tester un marquage de task à ”Done == true” avec la requêtecurl:
> curl --request POST --data'1'http://localhost:8080/done
7. Vous pouvez vérifier le fonctionnement de votre serveur en ouvrant dansvotre navigateur les adresses:

> - http://localhost:8080/done_ (le navigateur fait une requête HTTPGET)
> - http://localhost:8080

