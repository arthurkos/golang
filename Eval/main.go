//
// Arthur VIVANCOS
// MSI DO A
//
package main

import (
	"fmt"
	//"log"
	//"encoding/json"
	"io/ioutil"
	"net/http"
)

// Structure Task
type Task struct {
	Description string
	Done        bool
}

//Variable global tasks
var tasks []Task

//fonction main
func main() {

	http.HandleFunc("/", list)
	http.HandleFunc("/done", done)
	http.HandleFunc("/add", add)

	http.ListenAndServe(":8080", nil)
}

//
// fonction list
//
type Liste struct {
	ID   int
	Task string
}

func list(rw http.ResponseWriter, _ *http.Request) {

	list := []Liste{}
	task := []Task{
		{"Faire les courses", false},
		{"Payer les factures", false},
	}
	for id, i := range task {
		if !i.Done {
			list = append(list, Liste{id, i.Description})
		}
	}
	rw.WriteHeader(http.StatusOK)
}

//
// fonction add
//
func add(rw http.ResponseWriter, r *http.Request) {

	if r.Method == http.MethodPost {
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			fmt.Printf("Errorreadingbody:%v", err)
			http.Error(rw, "can'treadbody", http.StatusBadRequest)
			return
		}
		description := string(body)
		tasks = append(tasks, Task{description, false})
		rw.WriteHeader(http.StatusOK)
	}
	rw.WriteHeader(http.StatusBadRequest)

}

//
// fonction done
//
func done(rw http.ResponseWriter, r *http.Request) {

	//fonction case
	switch r.Method {
	case http.MethodGet:
		// je fais un print car je sais pas comment faire :(
		fmt.Println("MethodGet")
	case http.MethodPost:
		fmt.Println("MethodPost")
	default:
		rw.WriteHeader(http.StatusBadRequest)
	}

}
