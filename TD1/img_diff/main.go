package main

import (
	"crypto/sha256"
	"fmt"
	"io/ioutil"
	"log"
)

func readimg(path string) int {
	content, err := ioutil.ReadFile(path)
	if err != nil {
		log.Fatal(err)
	}
	h := sha256.New()
	result, err := h.Write([]byte(content))
	return result
}

func main() {

	image1 := readimg("image_1.jpg")
	image2 := readimg("image_2.jpg")
	image3 := readimg("image_3.jpg")

	fmt.Println(image1)
	fmt.Println(image2)
	fmt.Println(image3)

	if image1 == image2 && image1 != image3 {
		fmt.Println("image3 est differente")
	} else if image1 != image2 && image1 != image3 {
		fmt.Println("image1 est differente")
	} else {
		fmt.Println("image2 est differente")
	}

}
