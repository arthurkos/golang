package main

import (
	//"log"
	//"encoding/json"
	"net/http"
	//"io/ioutil"
)

// Structure Task
type Task struct {
	Description     string 
	Done		bool
}

//Variable global tasks
var tasks = make(map[string]Task)

//handler 


//fonction main
func main(){

//HandleFunc
	http.HandleFunc("/", list)
	http.HandleFunc("/done", done)
	http.HandleFunc("/add", add)

// lancement du srv web
	http.ListenAndServe(":8080", nil)
}
